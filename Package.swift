// swift-tools-version:5.0

import PackageDescription

let package = Package(
    name: "CoreMind",
    platforms: [
        .macOS(.v10_12), .iOS(.v10), .watchOS(.v4), .tvOS(.v10)
    ],
    products: [        
        .library(name: "CoreMind", targets: ["CoreMind"])
    ],
    dependencies: [

    ],
    targets: [
        .target(name: "CoreMind", dependencies: []),
        .testTarget(name: "CoreMindTests", dependencies: ["CoreMind"])
    ]
)
