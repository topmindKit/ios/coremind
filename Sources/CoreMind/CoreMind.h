//
//  CoreMind.h
//  CoreMind
//
//  Created by Martin Gratzer on 23/08/2016.
//  Copyright © 2016 Martin Gratzer. All rights reserved.
//

@import Foundation;

//! Project version number for CoreMind.
FOUNDATION_EXPORT double CoreMindVersionNumber;

//! Project version string for CoreMind.
FOUNDATION_EXPORT const unsigned char CoreMindVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreMind/PublicHeader.h>
